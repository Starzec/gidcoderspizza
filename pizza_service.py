from db.model import User


def vote_for_pizza(pizza_id: int, user_id: int, session) -> dict:
    """
    If user has not voted yet then pizza with given id receives vote

    :param pizza_id: the id of pizza that was voted on
    :param user_id: the user id who voted
    :param session: sqlalchemy.orm.session.Session
    :return: dictionary with message in case of error or updated pizza as dictionary
    """
    if user_id is None or pizza_id is None:
        return {'message': 'Wrong body form passed, user_id and pizza_id are obligatory'}
    user = session.query(User).get(user_id)
    if not user:
        return {'message': 'User with given id does not exist'}
    if user.pizza:
        return {'message': 'Given user already voted for the best pizza'}
    user.pizza_id = pizza_id
    session.flush()
    session.refresh(user)
    return user.pizza.as_json()
