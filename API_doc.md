# Pizza

I have prepared 10 users in default db with ids between 1-10 \
First five users (ids between 1-5) have already voted so they can not vote anymore. \
Next five users (ids between 6-10) have not voted yet so they can be used to vote. \

There is also 4 types of pizza which have ids between 1-4

## URL

`/pizza/vote`

- **Method:**

  `PUT`

- **Request Body**

```json
{
    "user_id": 6,
    "pizza_id": 2
}
```

- **Content type:** `application/json`


## Success Response

**Code** : `200 OK`

**Content type:** `json`

**Content examples**

```json
{
    "name": "Margherita Pizza",
    "pizza_id": 2,
    "score": 4,
    "toppings": [
        {
            "name": "Garlic",
            "topping_id": 6
        },
        {
            "name": "Bacon",
            "topping_id": 5
        },
        {
            "name": "Pepperoni",
            "topping_id": 1
        }
    ]
}
```

## Error Response

**Code** : `400 BAD REQUEST`

**Content type:** `json`

**Content examples**

Passed user_id or pizza_id as null or not passed

```json
{
    "message": "Wrong body form passed, user_id and pizza_id are obligatory"
}
```

**Code** : `400 BAD REQUEST`

**Content type:** `json`

**Content examples**

Passed user_id which doesn't exist

```json
{
    "message": "User with given id does not exist"
}
```

**Code** : `400 BAD REQUEST`

**Content type:** `json`

**Content examples**

User with given id already has voted

```json
{
    "message": "Given user already voted for the best pizza"
}
```