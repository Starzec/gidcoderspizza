import argparse
import json
import os

from flask import Flask, make_response, request
from db.database import Database
from pizza_service import vote_for_pizza

app = Flask(__name__)
parser = argparse.ArgumentParser()


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(
                Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class ConfigurationManager(metaclass=Singleton):
    database = None

    with open('config.json') as conf_file:
        configuration_file_dict = json.load(conf_file)
        PORT_TO_SERVE = configuration_file_dict['port']
        DATABASE_PROTOCOL = configuration_file_dict['database_protocol']
        DATABASE_ADDRESS = configuration_file_dict['database_path']


# As long as we omit authorization and authentication I am gonna expect for user id in request body
@app.route('/pizza/vote', methods=['PUT'])
def pizza_voter():
    data = request.get_json()
    user_id = data.get('user_id', None)
    pizza_id = data.get('pizza_id', None)
    with ConfigurationManager.database.session() as session:
        result = vote_for_pizza(pizza_id, user_id, session)
    if 'message' in result:
        return make_response(result, 400)
    return make_response(result, 200)


def initialize_arguments():
    with open('app_docs.json') as documentation_file:
        documentation = json.load(documentation_file)
        for option, description in documentation.items():
            parser.add_argument(option,
                                nargs='?',
                                help=description)


def parameters_filter(parameter: str):
    if parameter == "release":
        app.run(debug=False, port=ConfigurationManager.PORT_TO_SERVE)
    elif parameter == "debug":
        app.run(debug=True, port=ConfigurationManager.PORT_TO_SERVE)


def main():
    initialize_arguments()
    arguments = parser.parse_args()
    if arguments.mode:
        ConfigurationManager.database = Database(ConfigurationManager.DATABASE_PROTOCOL,
                                                 ConfigurationManager.DATABASE_ADDRESS)
        parameters_filter(arguments.mode)
    else:
        os.system('python app.py -h')


if __name__ == '__main__':
    main()
