**Release mode assumes that you have defined database** \
**Debug mode creates brand new database**

### Start release mode
To start application in release mode you have to type command: \
 - On Linux `sh run_app_release.sh`
 - On Windows `run_app_release.bat`
 
### Start debug mode
To start application in debug mode you have to type command: \
 - On Linux `sh run_app_debug.sh`
 - On Windows `run_app_debug.bat`
 
 # Manual start
 
 ## Linux
 
 ### Install requirements
 In root directory type command: \
 `pip3 install -r requirements.txt`
 
 ### Generate database
 In root directory type command: \
`python3 -m db.generate_data` \
It should generate `sqlite.db` file in "db" folder

### Run application
In root directory type command: \
`python3 app.py release` or for debug mode \
`python3 app.py debug`

## Windows

 ### Install requirements
 In root directory type command: \
 `pip3 install -r requirements.txt`
 
 ### Generate database
 In root directory type command: \
`python -m db.generate_data` \
It should generate `sqlite.db` file in "db" folder

### Run application
In root directory type command: \
`python app.py release` or for debug mode \
`python app.py debug`