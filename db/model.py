from sqlalchemy import Column, Integer, String, DateTime, func, ForeignKey
from sqlalchemy.orm import relationship
from .database import Database

Base = Database.Base


class Topping(Base):
    __tablename__ = 'toppings'

    id = Column(Integer, primary_key=True)
    name = Column(String(30, collation='NOCASE'), unique=True, nullable=False)

    pizzas = relationship('Pizza', secondary='pizza_topping')

    def as_json(self):
        return {
            'topping_id': self.id,
            'name': self.name
        }


class Pizza(Base):
    __tablename__ = 'pizzas'

    id = Column(Integer, primary_key=True)
    name = Column(String(30, collation='NOCASE'), unique=True, nullable=False)

    toppings = relationship('Topping', secondary='pizza_topping')
    voted_users = relationship('User', back_populates='pizza')

    def as_json(self):
        return {
            'pizza_id': self.id,
            'name': self.name,
            'score': len(self.voted_users),
            'toppings': [topping.as_json() for topping in self.toppings]
        }


class PizzaTopping(Base):
    __tablename__ = 'pizza_topping'

    id = Column(Integer, primary_key=True)
    time_created = Column(DateTime(timezone=True), server_default=func.now())
    pizza_id = Column(Integer, ForeignKey('pizzas.id'), nullable=False)
    topping_id = Column(Integer, ForeignKey('toppings.id'), nullable=False)


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    pizza_id = Column(Integer, ForeignKey('pizzas.id'), nullable=True)

    pizza = relationship('Pizza', back_populates='voted_users')

    def as_json(self):
        return {
            'user_id': self.id
        }
