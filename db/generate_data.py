import json
import os

from .database import Database
from .model import Topping, Pizza, PizzaTopping, User


def insert_data(session):
    toppings_names = [
        'Pepperoni',
        'Ham',
        'Pineapple',
        'Mozzarella',
        'Bacon',
        'Garlic'
    ]
    toppings = [Topping(name=name) for name in toppings_names]
    session.add_all(toppings)

    pizza_names = [
        'Meat Pizza',
        'Margherita Pizza',
        'Hawaiian Pizza',
        'Buffalo Pizza'
    ]
    pizzas = [Pizza(name=name) for name in pizza_names]
    session.add_all(pizzas)
    session.flush()

    pizza_toppings = [
        (1, 1),
        (1, 2),
        (2, 6),
        (2, 5),
        (2, 1),
        (3, 3),
        (3, 1),
        (4, 4),
        (4, 2),
        (4, 5)
    ]
    pizza_toppings_models = [PizzaTopping(pizza_id=pizza_id, topping_id=topping_id)
                             for pizza_id, topping_id in pizza_toppings]
    session.add_all(pizza_toppings_models)

    session.add_all([User(pizza_id=2), User(pizza_id=2), User(pizza_id=1), User(pizza_id=3), User(pizza_id=2),
                     User(), User(), User(), User(), User()])


def create_database(protocol, name):
    if os.path.isfile(name):
        os.remove(name)
    database = Database(protocol, name, check_if_exists=False)
    database.create_database()
    with database.session() as session:
        insert_data(session)
        

if __name__ == '__main__':
    with open("config.json") as conf_file:
        conf_dict = json.load(conf_file)
        DATABASE_PROTOCOL = conf_dict["database_protocol"]
        DATABASE_FILE = conf_dict["database_path"]

    create_database(DATABASE_PROTOCOL, DATABASE_FILE)
