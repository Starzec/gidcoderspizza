import os.path
from contextlib import contextmanager

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base


def check_if_sqlite_database_exists(path):
    if not os.path.isfile(path):
        raise FileNotFoundError(f"Database file {path} has not been found")


class Database:
    Base = declarative_base()

    def __init__(self, protocol, path, check_if_exists=True):
        self.engine = create_engine(protocol + path)
        self.session_factory = scoped_session(sessionmaker(
            autocommit=False, autoflush=False, bind=self.engine))

        if check_if_exists and protocol.startswith("sqlite"):
            check_if_sqlite_database_exists(path)

    def create_database(self):
        Database.Base.metadata.create_all(bind=self.engine)

    @contextmanager
    def session(self):
        session = self.session_factory()

        try:
            yield session
            session.commit()
        except:
            session.rollback()
            raise
        finally:
            self.session_factory.remove()
